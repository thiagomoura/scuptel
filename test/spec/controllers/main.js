'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('scuptelApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  beforeEach(function() {
      scope.plans = [
        {
          plan: 'FaleMais 30',
          time: '30'
        },
        {
          plan: 'FaleMais 60',
          time: '60'
        },
        {
          plan: 'FaleMais 120',
          time: '120'
        }
      ];

      scope.dddPricing = [
        {
          origin: '011',
          destiny: '016',
          price: '1.90'
        }
      ];

      scope.dddDetails = [
        {
          ddd: '011',
          city: 'São Paulo'
        },
        {
          ddd: '016',
          city: 'Ribeirão Preto'
        }
      ];

  });

  it('Should return the price and the plan name for each plan', function () {
    scope.formData.from = '011';
    scope.formData.to = '016';
    scope.formData.minutes = '100';
    scope.calculatePlans();

    expect(scope.result.length).toBe(4);

    expect(scope.result[0].plan).toBe('FaleMais 30');
    expect(scope.result[0].price).toBe('146.30');

    expect(scope.result[1].plan).toBe('FaleMais 60');
    expect(scope.result[1].price).toBe('83.60');

    expect(scope.result[2].plan).toBe('FaleMais 120');
    expect(scope.result[2].price).toBe(0);

    expect(scope.result[3].plan).toBe('Sem FaleMais');
    expect(scope.result[3].price).toBe(190);
  });
});
