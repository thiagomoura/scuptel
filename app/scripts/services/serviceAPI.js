'use strict';

angular
  .module('scuptelApp').factory('serviceAPI', function($http, config) {
    var _getAPI = function (method) {
      console.log('Get ' + method, config[method]);
      return $http.get(config[method]);
    };

    return {
      _getAPI: _getAPI
    };
  });
