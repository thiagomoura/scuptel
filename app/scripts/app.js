'use strict';
/**
 * @ngdoc overview
 * @name scuptelApp
 * @description
 * # Projeto de teste desenvolvido por Thiago Moura
 *
 */
angular
  .module('scuptelApp', [
    'ngRoute',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
