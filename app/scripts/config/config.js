'use strict';

angular
  .module('scuptelApp').constant('config', {
      pricing: 'http://private-fe2a-scuptel.apiary-mock.com/ddd/pricing',
      details: 'http://private-fe2a-scuptel.apiary-mock.com/ddd/details',
      plans: 'http://private-fe2a-scuptel.apiary-mock.com/plans'
  });
