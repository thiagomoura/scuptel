'use strict';

/**
 * @ngdoc function
 * @name scuptelApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the scuptelApp
 */
angular.module('scuptelApp')
  .controller('MainCtrl', function ($scope, serviceAPI, $modal) {
    // API MODEL DATA
    $scope.plans = [];
    $scope.dddPricing = [];
    $scope.dddDetails = [];
    $scope.result = [];

    // SCOPE VARS
    $scope.formMethod = null;
    $scope.formData = [];

    // SCOPE ERROR MESSAGE
    $scope.errorMessage = null;

    $scope.loadPlans = function () {
      serviceAPI._getAPI('plans').success(function (data) {
        console.log('Result plans ', data.data);
        $scope.plans = data.data;
      }).error(function () {
        $scope.errorMessage = 'Desculpe, mas tivemos problemas ao carregar os planos!';
      });
    };

    $scope.loadDDDPricing = function () {
      serviceAPI._getAPI('pricing').success(function (data) {
        console.log('Result pricing ', data.data);
        $scope.dddPricing = data.data;
      }).error(function () {
        console.log('ERRO');
        $scope.errorMessage = 'Desculpe, mas tivemos problemas ao carregar os preços!';
      });
    };

    $scope.loadDDDDetails = function () {
      serviceAPI._getAPI('details').success(function (data) {
        console.log('Result details ', data.data);
        $scope.dddDetails = data.data;
      }).error(function () {
        $scope.errorMessage = 'Desculpe, mas tivemos problemas ao nossa tabela de DDD!';
      });
    };

    $scope.loadPlans();
    $scope.loadDDDPricing();
    $scope.loadDDDDetails();

    $scope.openModal = function (method) {
      $scope.formMethod = method;
      $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'views/modal.html',
        scope: $scope,
        controller: ['$scope', '$modalInstance', function($scope, $modalInstance) {

          $scope.cancel = function() {
            $modalInstance.close();
          };

          $scope.itemSelected = function (item) {
            $scope.formData[$scope.formMethod] = item;
            $scope.formMethod = null;
            $modalInstance.close();
          };

        }],
        windowClass: 'app-modal-window',
        keyboard: true
      });
    };

    $scope.calculateValuePerPlan = function (plan) {
      var selectObjPrice = _.where($scope.dddPricing, {origin: $scope.formData.from, destiny: $scope.formData.to});
      var price;

      if (($scope.formData.from === '011' || $scope.formData.to  === '011') && ($scope.formData.from !== $scope.formData.to)) {
        if (plan.plan === 'Sem FaleMais') {
          price = $scope.formData.minutes * parseFloat(selectObjPrice[0].price);
        } else if ($scope.formData.minutes < parseInt(plan.time)) {
          price = 0;
        } else {
          price = ((($scope.formData.minutes - parseInt(plan.time)) * parseFloat(selectObjPrice[0].price)) * 1.10).toFixed(2);
        }
      }

      return {
          plan: plan.plan,
          price: price
        };
    };

    $scope.calculatePlans = function () {
      if ($scope.formData.from && $scope.formData.to && $scope.formData.minutes) {
        $scope.result = [];
        $.each($scope.plans, function (index, plan) {
          var planValues = $scope.calculateValuePerPlan(plan);
          $scope.result.push(planValues);
        });
        var withoutPlanValues = $scope.calculateValuePerPlan({
          plan: 'Sem FaleMais',
          time: '0'
        });
        $scope.result.push(withoutPlanValues);
      }
    };

    $scope.$watch(function() {
      return $scope.formData.to;
    }, function() {
      $scope.calculatePlans();
    });

    $scope.$watch(function() {
      return $scope.formData.from;
    }, function() {
      $scope.calculatePlans();
    });

    $scope.$watch(function() {
      return $scope.formData.minutes;
    }, function() {
      $scope.calculatePlans();
    });
  });
