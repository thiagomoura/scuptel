# Scuptel FaleMais “Show me the code”

Autor: Thiago Moura ([thiiix.xbox@gmail.com](mailto:thiiix.xbox@gmail.com)  | [Facebook](http://facebook.com/thiiix))

Projeto criado com [yo angular generator](https://github.com/yeoman/generator-angular)
versão 0.11.1.

## Build & development

Execute `npm install` para instalar as dependências.

Depois execute `bower install` para instalar as libs javascript.

É necessário ter a gem "Compass" instalada também, caso não tenha, execute `gem install compass`.

Por fim, execute `grunt` para gerar a aplicação e  `grunt serve` para um preview.

## Testing

Execute `grunt test` para rodar todos os testes unitários com karma.